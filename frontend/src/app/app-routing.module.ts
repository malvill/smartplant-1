import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SystemStateComponent} from "./system-state/system-state.component";
import {ProfilesComponent} from "./profiles/profiles.component";

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full'},
  { path: 'main', component: SystemStateComponent },
  { path: 'profiles', component: ProfilesComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
