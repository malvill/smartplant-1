import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PlantProfileComponent } from './profiles/plant-profile/plant-profile.component';
import { ChartComponent } from './system-state/chart-controls/chart/chart.component';
import { SensorComponent } from './system-state/sensors/sensor/sensor.component';
import { SystemStateComponent } from './system-state/system-state.component';
import { ProfilesComponent } from './profiles/profiles.component';
import { SensorsComponent } from './system-state/sensors/sensors.component';
import { NgChartsModule } from "ng2-charts";
import { PlantLifeComponent } from './system-state/plant-life/plant-life.component';
import {ChartControlsComponent} from "./system-state/chart-controls/chart-controls.component";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavigationComponent,
    PlantProfileComponent,
    ChartComponent,
    SensorComponent,
    SystemStateComponent,
    ProfilesComponent,
    SensorsComponent,
    PlantLifeComponent,
    ChartControlsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgChartsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
