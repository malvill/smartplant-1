import {Sensor} from "./sensor.model";
import {SensorAccess} from "./sensor-access.model";

export class SensorsData {
  constructor(public waterTemperature: Sensor,
              public airTemperature: Sensor,
              public ph: Sensor,
              public ec: Sensor,
              public humidity: Sensor) {
  }
}

export class SensorsHistoryErrorPossible {
  constructor(public airTemperature: Sensor[] | Error,
              public ph: Sensor[] | Error,
              public ec: Sensor[] | Error,
              public waterTemperature?: Sensor[] | Error,
              public humidity?: Sensor[] | Error) {
  }
}

export class SensorsHistory {
  constructor(public airTemperature: Sensor[],
              public ph: Sensor[],
              public ec: Sensor[],
              public waterTemperature?: Sensor[],
              public humidity?: Sensor[]) {
  }
}

