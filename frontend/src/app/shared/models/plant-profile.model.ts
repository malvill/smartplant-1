export class PlantProfile {
  constructor(public imagePath: string, public plantName: string, public plantSort: string,
              public growingPeriod: Array<number>, public daySummerTemperatures: Array<number>, public dayWinterTemperatures: Array<number>,
              public nightTemperatures: Array<number>, public humidityRange: Array<number>, public phRange: Array<number>) {
  }
}
