export class SensorRaw {
  constructor(public time: string,
              public value: number) {
  }
}

export class Sensor {
  constructor(public time: Date,
              public value: number) {
  }
}

