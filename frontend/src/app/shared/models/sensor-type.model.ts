export class SensorType {
  constructor(public name: string,
              public unit: string,
              public value?: number) {
  }
}
