export class SensorAccess {
  constructor(public uniqueID: string,
              public unit: string,
              public channel: number) {}
}

export class AllSensorsAccess {
  constructor(public waterTemperature: SensorAccess,
              public airTemperature: SensorAccess,
              public ph: SensorAccess,
              public ec: SensorAccess,
              public humidity: SensorAccess) {
  }
}
