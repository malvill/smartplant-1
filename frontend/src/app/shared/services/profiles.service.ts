import {Injectable} from "@angular/core";
import {PlantProfile} from "../models/plant-profile.model";

@Injectable({
  providedIn: "root"
})
export class ProfilesService {
  private plantProfiles: PlantProfile[] = [
    {imagePath: '../../../assets/images/plant-profiles/batavia.jpeg', plantName: 'батавия', plantSort: 'ОРФЕЙ', growingPeriod: [30, 40], daySummerTemperatures: [20.0, 27.0], dayWinterTemperatures: [18.0, 22.0],
      nightTemperatures: [15.0, 19.0], humidityRange: [60, 100], phRange: [5.5, 6.5]},
    {imagePath: '../../../assets/images/plant-profiles/lollo-rossa.jpeg', plantName: 'лолло росса', plantSort: '', growingPeriod: [30, 40], daySummerTemperatures: [20.0, 27.0], dayWinterTemperatures: [18.0, 22.0],
      nightTemperatures: [15.0, 19.0], humidityRange: [60, 100], phRange: [5.5, 6.5]}
  ]

  getProfiles() {
    return this.plantProfiles.slice()
  }
}
