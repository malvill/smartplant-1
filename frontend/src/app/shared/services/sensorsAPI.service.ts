import {Injectable} from "@angular/core";
import {SensorsData, SensorsHistoryErrorPossible} from "../models/sensors.model";
import 'rxjs/Rx';
import "rxjs/add/operator/retry";
import 'rxjs/add/operator/catch';
import {map, retry} from 'rxjs/operators';
import {forkJoin, Observable, of, pipe, Subject} from "rxjs";
import {HttpClient, HttpErrorResponse, HttpHeaders} from "@angular/common/http";
import {Sensor, SensorRaw} from "../models/sensor.model";
import {AllSensorsAccess, SensorAccess} from "../models/sensor-access.model";
import {SensorsService} from "./sensors.service";
import {userError} from "@angular/compiler-cli/src/transformers/util";

@Injectable({
  providedIn: 'root'
})
export class SensorsAPIService {

  private apiKey = '9PZOXkPhFBeqSGmM9ztTv6Ur08BR1VXfbZolkQyDOkzIpJSA/B62d01+9mKVL9Ew/nVRKaca+ba9eEjMSs4gkGadAOtI2xfKyJFInCob4TDBZW05S6MuH2rr8iERlS7gF9UdJMCDU64Nq67FfXi+l9cJ8PDHzrfcQnVcXdPnAoo=';
  private requestOptions = {
    headers: new HttpHeaders({'Authorization': this.apiKey})
  }
  private hostUrl = `http://localhost:8000`;
  private lastMeasurementsUrl = `${this.hostUrl}/api/operation/post_measurements_create/measurements/last`;
  private historyUrl = `${this.hostUrl}/api/measurements/past`

  private sensorsAccess: AllSensorsAccess = {
    airTemperature: {uniqueID: '312de4dc-0866-4109-9d24-2201380d834c', unit: 'temperature', channel: 0},
    waterTemperature: {uniqueID: 'afb47470-4204-4139-b0c5-3abffafe6247', unit: 'temperature', channel: 0},
    ph: {uniqueID: 'afb47470-4204-4139-b0c5-3abffafe6247', unit: 'ion-concentration', channel: 1},
    ec: {uniqueID: '8b83fa2f-809f-416c-95a7-0aadb29b0b77', unit: 'microsiemens-per-sentimeter', channel: 0},
    humidity: {uniqueID: '312de4dc-0866-4109-9d24-2201380d834c', unit: 'percent', channel: 1}
  }

  updateRangeInSeconds = 60;
  historyRangeInSeconds = 172800;

  constructor(private sensorsService: SensorsService,
              private http: HttpClient) {
  }

  fetchLatestForSensor(sensor: SensorAccess): Observable<Sensor | Error> {
    let url = `${this.lastMeasurementsUrl}/${sensor.uniqueID}/${sensor.unit}/${sensor.channel}/${this.updateRangeInSeconds}`;
    // return this.http.get(url, this.requestOptions)
    return this.http.get(url)
      .pipe(map((raw: SensorRaw) => {
        return this.normalizeDate(raw);
      }))
      .catch((err: HttpErrorResponse) => {
          console.log(err);
          return of(new Error());
        }
      )
  }

  fetchHistoryForSensor(sensor: SensorAccess): Observable<Sensor[] | Error> {
    let url = `${this.historyUrl}/${sensor.uniqueID}/${sensor.unit}/${sensor.channel}/${this.historyRangeInSeconds}`;
    // return this.http.get(url, this.requestOptions)
    return this.http.get(url)
      .pipe(map((raw: {measurements: SensorRaw[]}) => {
          const history = [];
          for (const measurement of raw.measurements) {
            history.push(this.normalizeDate(measurement));
          }
          return history;
        })
      )
      .catch((err: HttpErrorResponse) => {
        console.log(err);
        return of(new Error());
      })
  }

  private normalizeDate(measurement: SensorRaw): Sensor {
    let date = new Date(measurement['time']);
    date.setSeconds(0);
    return new Sensor(date, measurement.value);
  }

  fetchLatestForAllSensors() {
    forkJoin({
      airTemperature: this.fetchLatestForSensor(this.sensorsAccess.airTemperature),
      waterTemperature: this.fetchLatestForSensor(this.sensorsAccess.waterTemperature),
      ph: this.fetchLatestForSensor(this.sensorsAccess.ph),
      ec: this.fetchLatestForSensor(this.sensorsAccess.ec),
      humidity: this.fetchLatestForSensor(this.sensorsAccess.humidity),
    }).subscribe(
      (sensors: SensorsData) => {
        console.log(sensors);
        this.sensorsService.setLatest(sensors);
      }
    )
  }

  fetchHistoryForAllSensors() {
    forkJoin({
      airTemperature: this.fetchHistoryForSensor(this.sensorsAccess.airTemperature),
      ph: this.fetchHistoryForSensor(this.sensorsAccess.ph),
      ec: this.fetchHistoryForSensor(this.sensorsAccess.ec),
      humidity: this.fetchHistoryForSensor(this.sensorsAccess.humidity)
    }).subscribe(
      (sensors: SensorsHistoryErrorPossible) => {
        console.log(sensors);
        this.sensorsService.setHistory(sensors);
      }
    )
  }

}
