import {Injectable} from "@angular/core";
import {Subject} from "rxjs";
import {SensorsData, SensorsHistoryErrorPossible} from "../models/sensors.model";


@Injectable({
  providedIn: "root"
})
export class SensorsService {

  current: SensorsData;

  history: SensorsHistoryErrorPossible = {
    airTemperature: [{
      time: new Date(),
      value: 24.0
    }],
    waterTemperature: [{
      time: new Date(),
      value: 24.0
    }],
    ph: [{
      time: new Date(),
      value: 7.0
    }],
    ec: [{
      time: new Date(),
      value: 1.0
    }],
    humidity: [{
      time: new Date(),
      value: 89
    }]
  };

  currentChanged = new Subject<SensorsData>();
  historyChanged = new Subject<SensorsHistoryErrorPossible>();

  setLatest(sensors: SensorsData) {
    this.current = sensors;
    this.updateHistory(sensors);
    this.currentChanged.next(this.current);
    this.historyChanged.next(this.history);
  }

  setHistory(sensors: SensorsHistoryErrorPossible) {
    this.history = sensors;
    this.historyChanged.next(this.history)
  }

  updateHistory(sensorsLatestList: SensorsData) {
    updateSensorHistory('ph', this.history);
    updateSensorHistory('ec', this.history);
    updateSensorHistory('humidity', this.history);
    updateSensorHistory('airTemperature', this.history);

    function updateSensorHistory(sensorName, history) {
      if (sensorsLatestList[sensorName] instanceof Error) return;
      history[sensorName] = history[sensorName].slice(1);
      history[sensorName].push(sensorsLatestList[sensorName])
    }
  }


}
