import {Component, Input, OnInit} from '@angular/core';
import {PlantProfile} from "../../shared/models/plant-profile.model";

@Component({
  selector: 'app-plant-profile',
  templateUrl: './plant-profile.component.html',
  styleUrls: ['./plant-profile.component.scss']
})
export class PlantProfileComponent implements OnInit {
  @Input() plantProfile: PlantProfile;
  expanded: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggle() {
    this.expanded = !this.expanded;
  }

}
