import { Component, OnInit } from '@angular/core';
import {PlantProfile} from "../shared/models/plant-profile.model";
import {ProfilesService} from "../shared/services/profiles.service";

@Component({
  selector: 'app-profiles',
  templateUrl: './profiles.component.html',
  styleUrls: ['./profiles.component.scss']
})
export class ProfilesComponent implements OnInit {
  // @ts-ignore
  plantProfiles: PlantProfile[];

  constructor(private profilesService: ProfilesService) { }

  ngOnInit(): void {
    this.plantProfiles = this.profilesService.getProfiles();
  }

}
