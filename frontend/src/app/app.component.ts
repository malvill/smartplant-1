import {Component, OnInit} from '@angular/core';
import {SensorsAPIService} from "./shared/services/sensorsAPI.service";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{

  constructor(private sensorsAPIService: SensorsAPIService) {
  }

  ngOnInit() {
    this.sensorsAPIService.fetchHistoryForAllSensors();
    this.sensorsAPIService.fetchLatestForAllSensors();
    setInterval(this.sensorsAPIService.fetchLatestForAllSensors, 60000)
  }
}
