import {Component, EventEmitter, Output} from '@angular/core';
import {SensorType} from "../../shared/models/sensor-type.model";



@Component({
  selector: 'app-chart-controls',
  templateUrl: './chart-controls.component.html',
  styleUrls: ['./chart-controls.component.scss']
})
export class ChartControlsComponent {
  timeRange: number;
  sensorType: SensorType;

  onChangeTime(hours: number) {
    this.timeRange = hours;
  }

  onChangeSensor(sensorName, sensorUnit) {
    this.sensorType = new SensorType(sensorName, sensorUnit);
  }
}

