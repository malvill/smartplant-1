import {
  AfterViewInit,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {Sensor} from "../../../shared/models/sensor.model";
import {SensorsService} from "../../../shared/services/sensors.service";
import {Chart} from "chart.js";
import {Subscription} from "rxjs";
import {SensorsHistory} from "../../../shared/models/sensors.model";
import 'chartjs-adapter-moment';
import {SensorType} from "../../../shared/models/sensor-type.model";


type SensorChartData = Array<Array<[Date, number]>>;

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnInit, OnChanges, AfterViewInit, OnDestroy {
  @ViewChild('myChart') canvas: ElementRef;
  @Input('timeRange') timeRange: number;
  @Input('sensorType') sensorType: SensorType;
  chart: any;
  chartType: string;

  private subscription: Subscription;

  airTemperature: SensorChartData;
  humidity: SensorChartData;
  ec: SensorChartData;
  ph: SensorChartData;

  labels = [
    new Date(2021, 10, 15, 15, 0),
    new Date(2021, 10, 15, 15, 5),
    new Date(2021, 10, 15, 15, 10),
    new Date(2021, 10, 15, 15, 15),
    new Date(2021, 10, 15, 15, 20),
    new Date(2021, 10, 15, 15, 25),
    new Date(2021, 10, 15, 15, 30),
    new Date(2021, 10, 15, 15, 35),
    new Date(2021, 10, 15, 15, 40),
    new Date(2021, 10, 15, 15, 45),
    new Date(2021, 10, 15, 15, 50),
    new Date(2021, 10, 15, 15, 55),
    new Date(2021, 10, 15, 16, 0),
  ];

  constructor(private sensorsService: SensorsService) {
  }

  ngOnInit(): void {
    this.subscription = this.sensorsService.historyChanged
      .subscribe(
        (history: SensorsHistory) => {
          this.airTemperature = this.transformSensorHistory(history['airTemperature']);
          this.humidity = this.transformSensorHistory(history['humidity']);
          this.ec = this.transformSensorHistory(history['ec']);
          this.ph = this.transformSensorHistory(history['ph']);
          this.updateChart(this.airTemperature[0]);
        },
      );
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const property in changes) {
      if (property === 'timeRange') {
        this.onChangeTimeRange(changes[property].currentValue);
      } else if (property === 'sensorType') {
        this.onChangeSensorType(changes[property].currentValue)
      }
    }
  }

  transformSensorHistory(sensorHistory: Sensor[]) {
    const timestamps = [];
    const values = [];
    for (const measurement of sensorHistory) {
      timestamps.push(measurement[0]);
      values.push(measurement[1])
    }
    return([timestamps, values]);
  }


  ngAfterViewInit(): void {
    this.chartMethod();
  }

  onChangeTimeRange(hours: number) {
    // this.labels =
  }

  onChangeSensorType(sensorType: SensorType) {

  }

  updateChart(xLabels) {
    this.chart.labels
    this.chart.update();
  }

  chartMethod() {
    this.chart = new Chart(this.canvas.nativeElement, {
      type: 'line',
      data: {
        labels: this.labels,
        datasets: [
          {
            label: 'Sell per week',
            fill: false,
            backgroundColor: 'rgba(75,192,192,0.4)',
            borderColor: '#627A5C',
            borderCapStyle: 'butt',
            borderDash: [],
            borderDashOffset: 0.0,
            borderJoinStyle: 'miter',
            pointBorderColor: '#627A5C',
            pointBackgroundColor: '#fff',
            pointBorderWidth: 3,
            pointHoverRadius: 5,
            pointHoverBackgroundColor: '#fff',
            pointHoverBorderColor: '#627A5C',
            pointHoverBorderWidth: 3,
            pointRadius: 1,
            pointHitRadius: 10,
            data: this.generateSeries(),
            spanGaps: false,
          }
        ]
      },
      options: {
        elements: {
          line: {
            tension: 0.6
          }
        },
        scales: {
          xAxes: {
            type: 'time',

            ticks: {
              source: 'labels', // get ticks from given labels
              font: {
                family: "'Poppins', sans-serif",
                size: 14,
                lineHeight: 2.2
              }
            },

            time: {
              minUnit: 'minute', // smallest time format

              displayFormats: {
                minute: "HH: mm",
                hour: "HH: mm",
                day: "dd/MM",
                week: "dd/MM",
                month: "MMMM yyyy",
                quarter: 'MMMM yyyy',
                year: "yyyy",
              }
            },

            grid: {
              display: false
            }
          },
          yAxes: {
            ticks: {
              font: {
                family: "'Poppins', sans-serif",
                size: 14,
                lineHeight: 2.2
              }
            }
          }
        },
        plugins: {
          legend: {
            display: false,
          },
          tooltip: {
            position: "nearest",
            // external: this.externalTooltipHandler
            backgroundColor: '#fff',
            bodyFont: {
              size: 12,
              family: "'Poppins', sans-serif",
              lineHeight: 2.8
            },
            bodyColor: '#11263C'
          }
        },
        interaction: {
          intersect: false,
          mode: "index"
        },
      }
    });
  }



  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  generateSeries() {
    let series = [];
    let val = 0;

    let start = new Date(2021, 10, 15, 15);
    let end = new Date(2021, 10, 15, 16);

    while (start < end) {
      val += Math.floor(Math.random() * 11) - 5;

      series.push({
        "x": start,
        "y": val,
      });

      start = new Date(start.getTime() + 60000);
    }

    return series;
  }

  obj  = {1566656100000: [new Date("2019-08-24T14:15:22Z"), 16]}


  getOrCreateTooltip = (chart) => {
    let tooltipEl = chart.canvas.parentNode.querySelector('div');

    if (!tooltipEl) {
      tooltipEl = document.createElement('div');
      tooltipEl.style.background = '#fff';
      tooltipEl.style.borderRadius = '5px';
      tooltipEl.style.color = '#11263C';
      tooltipEl.style.pointerEvents = 'none';
      tooltipEl.style.position = 'absolute';
      tooltipEl.style.transition = 'all .1s ease';
      tooltipEl.style.boxShadow = '0px 8px 8px rgba(50, 50, 71, 0.08), 0px 8px 16px rgba(50, 50, 71, 0.06)';

      const table = document.createElement('table');
      table.style.margin = '0px';

      tooltipEl.appendChild(table);
      chart.canvas.parentNode.appendChild(tooltipEl);
    }

    return tooltipEl;
  };

  externalTooltipHandler = (context) => {
    // Tooltip Element
    const {chart, tooltip} = context;
    const tooltipEl = this.getOrCreateTooltip(chart);

    // Hide if no tooltip
    if (tooltip.opacity === 0) {
      tooltipEl.style.opacity = 0;
      return;
    }

    // Set Text
    if (tooltip.body) {
      const titleLines = tooltip.title || [];
      const bodyLines = tooltip.body.map(b => b.lines);

      const tableHead = document.createElement('thead');

      titleLines.forEach(title => {
        const tr = document.createElement('tr');
        tr.style.borderWidth = '0';

        const th = document.createElement('th');
        th.style.borderWidth = '0';
        const text = document.createTextNode(title);

        th.appendChild(text);
        tr.appendChild(th);
        tableHead.appendChild(tr);
      });

      const tableBody = document.createElement('tbody');
      bodyLines.forEach((body, i) => {
        const colors = tooltip.labelColors[i];

        const span = document.createElement('span');
        span.style.background = colors.backgroundColor;
        span.style.borderColor = colors.borderColor;
        span.style.borderWidth = '2px';
        span.style.marginRight = '10px';
        span.style.height = '10px';
        span.style.width = '10px';
        span.style.display = 'inline-block';

        const tr = document.createElement('tr');
        tr.style.backgroundColor = 'inherit';
        tr.style.borderWidth = '0';

        const td = document.createElement('td');
        td.style.borderWidth = '0';

        const text = document.createTextNode(body);

        td.appendChild(span);
        td.appendChild(text);
        tr.appendChild(td);
        tableBody.appendChild(tr);
      });

      const tableRoot = tooltipEl.querySelector('table');

      // Remove old children
      while (tableRoot.firstChild) {
        tableRoot.firstChild.remove();
      }

      // Add new children
      tableRoot.appendChild(tableHead);
      tableRoot.appendChild(tableBody);
    }

    const {offsetLeft: positionX, offsetTop: positionY} = chart.canvas;

    // Display, position, and set styles for font
    tooltipEl.style.opacity = 1;
    tooltipEl.style.left = positionX + tooltip.caretX + 'px';
    tooltipEl.style.top = positionY + tooltip.caretY + 'px';
    tooltipEl.style.font = tooltip.options.bodyFont.string;
    tooltipEl.style.padding = tooltip.options.padding + 'px ' + tooltip.options.padding + 'px';
  };

}
