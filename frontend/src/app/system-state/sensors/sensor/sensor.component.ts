import {Component, Input, OnInit} from '@angular/core';
import {SensorType} from "../../../shared/models/sensor-type.model";

@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.scss']
})
export class SensorComponent implements OnInit {
  // @ts-ignore
  @Input() sensor: SensorType;

  constructor() { }

  ngOnInit(): void {
  }

}
