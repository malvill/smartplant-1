import {Component, OnDestroy, OnInit} from '@angular/core';
import {SensorsService} from "../../shared/services/sensors.service";
import {Sensor} from "../../shared/models/sensor.model";
import {Subscription} from "rxjs";
import {SensorType} from "../../shared/models/sensor-type.model";
import {SensorsData} from "../../shared/models/sensors.model";

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.scss']
})

export class SensorsComponent implements OnInit, OnDestroy {
  airTemperatureSensor: SensorType = {
    name: 'температура воздуха',
    unit: '&#176;C',
    value: 24.0
  };
  waterTemperatureSensor: SensorType = {
    name: 'температура воды',
    unit: '&#176;C',
    value: 24.0
  };
  phSensor: SensorType = {
    name: 'pH',
    unit: '',
    value: 24.0
  };
  ecSensor: SensorType = {
    name: 'EC',
    unit: 'мСм/см',
    value: 1.0
  };
  waterLvlSensor: SensorType = {
    name: 'вода',
    unit: '%',
    value: 50
  };
  solutionLvlSensor: SensorType = {
    name: 'раствор',
    unit: '%',
    value: 50
  };

  private subscription: Subscription;

  constructor(private sensorsService: SensorsService) { }

  ngOnInit(): void {
    this.subscription = this.sensorsService.currentChanged
      .subscribe(
        (sensor: SensorsData) => {
          console.log('airtempsensor', sensor, sensor['airTemperature'], sensor['airTemperature'].value)
          this.airTemperatureSensor.value = sensor['airTemperature'].value;
          this.waterTemperatureSensor.value = sensor['waterTemperature'].value;
          this.phSensor.value = sensor['ph'].value;
          this.ecSensor.value = sensor['ec'].value;
          this.waterLvlSensor.value = sensor['waterLvl'].value;
          this.solutionLvlSensor.value = sensor['solutionLvl'].value;
        }
      )
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

}
