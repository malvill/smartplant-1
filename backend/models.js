const sensorsAccess = {
    airTemperature: {uniqueID: '312de4dc-0866-4109-9d24-2201380d834c', unit: 'temperature', channel: 0},
    waterTemperature: {uniqueID: 'afb47470-4204-4139-b0c5-3abffafe6247', unit: 'temperature', channel: 0},
    ph: {uniqueID: 'afb47470-4204-4139-b0c5-3abffafe6247', unit: 'ion-concentration', channel: 1},
    ec: {uniqueID: '8b83fa2f-809f-416c-95a7-0aadb29b0b77', unit: 'microsiemens-per-sentimeter', channel: 0},
    humidity: {uniqueID: '312de4dc-0866-4109-9d24-2201380d834c', unit: 'percent', channel: 1}
}

class Measurement {
    constructor(datetime, value) {
        this.time = datetime || new Date();
        this.value = value;
    }

    toJson() {
        return {
            time: this.time.toUTCString(),
            value: this.value
        }
    }
}

class Sensor {

    constructor(unit) {
        this.lastMeasurement = {};
        this.history = [];
        this.unit = unit;
        this.initializeDataGeneration();
    }

    addMeasurement(time, value) {
        const measurement = new Measurement(time, value);
        this.lastMeasurement = measurement;
        this.history.push(measurement);
        return measurement;
    }

    generateRandomNumber(min, max) {
        return +(Math.random() * (max - min) + min).toFixed(1);
    }

    initializeDataGeneration() {
        const [min, max] = this.defineRange();
        setInterval(() => {
            this.addMeasurement(new Date(), this.generateRandomNumber(min, max))
        }, 60000)
    };

    defineRange() {
        const unit = this.unit;
        switch (unit) {
            case 'temperature':
                return [18, 24.5];
            case 'ion-concentration':
                return [4, 9];
            case 'microsiemens-per-sentimeter':
                return [1200, 1500];
            case 'percent':
                return [0, 100];
        }
    }
}

class SensorsManager {
    constructor() {
        this.sensors = {
            '312de4dc-0866-4109-9d24-2201380d834c': {
                0: {
                    temperature: new Sensor('temperature')
                },
                1: {
                    percent: new Sensor('percent')
                },
            },
            'afb47470-4204-4139-b0c5-3abffafe6247': {
                0: {
                    temperature: new Sensor('temperature')
                },
                1: {
                    'ion-concentration': new Sensor('ion-concentration')
                }
            },
            '8b83fa2f-809f-416c-95a7-0aadb29b0b77': {
                0: {
                    'microsiemens-per-sentimeter': new Sensor('microsiemens-per-sentimeter')
                }
            }
        }
    }

    getSensor(uniqueID, channel, unit) {
        return this.sensors[uniqueID][channel][unit];
    }
}

module.exports = {
    SensorsManager, Sensor, Measurement
}
