const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const models = require('./models');

class Application {
    constructor() {
        this.expressApp = express();
        this.manager = new models.SensorsManager();
        this.attachRoutes();
    }

    attachRoutes() {
        let app = this.expressApp;
        app.use(cors({
            origin: '*'
        }))
        let jsonParser = bodyParser.json();
        app.get('/api/operation/post_measurements_create/measurements/last/:uniqueID/:unit/:channel/60',
            this.getLatestHandler.bind(this));
        app.get('/api/measurements/past/:uniqueID/:unit/:channel/172800',
            this.getHistoryHandler.bind(this))
    }

    getLatestHandler(req, res) {
        const sensor = this.manager.getSensor(req.params.uniqueID, req.params.channel, req.params.unit);
        if (!sensor) {
            res.status(404).json({});
        } else {
            const latestJson = sensor.lastMeasurement.toJson();
            res.json(latestJson);
        }
    }

    getHistoryHandler(req, res) {
        const sensor = this.manager.getSensor(req.params.uniqueID, req.params.channel, req.params.unit);
        if (!sensor) {
            res.status(404).json({});
        } else {
            const historyJson = sensor.history.map(measurement => measurement.toJson());
            const response = {
                measurements: historyJson
            }
            res.json(response);
        }
    }
}

module.exports = Application;
